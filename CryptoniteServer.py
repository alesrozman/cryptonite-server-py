from twisted.web import server, resource
from twisted.internet import reactor, endpoints

from twisted.internet import ssl, protocol, task, defer
from twisted.python import log
from twisted.python.modules import getModule

import sys
import json
import time

from CryptoniteHandler import CryptoniteHandler
import logger


current_milli_time = lambda: int(round(time.time() * 1000))


class Server(resource.Resource):
    isLeaf = True
    
    def render_GET(self, request):
        print("GET")
        return "Certificate aquired successfuly"

    def render_POST(self, request):
        
        logger.info("POST --------------------------")
        
        # print request.requestHeaders

        postData = request.content.read()
                
        logger.info("Received: " + str(postData))
        
        handler = CryptoniteHandler()

        handler.context.start_time = current_milli_time()

        response = handler.handle_request(request, postData)

        handler.context.end_time = current_milli_time()

        response_string = str(response)
        
        # request.setHeader("Content-Length", len(responseString))
        logger.info("Sending response (" + str(len(response_string)) + ") (" +
                    str(handler.context.end_time - handler.context.start_time) + "ms): " + response_string)

        # logging for statistics
        # logger.info(handler.context.to_json())
        
        return response_string

# Start logging ??
logger.init()

#endpoints.serverFromString(reactor, "tcp:15987").listen(server.Site(Server()))
endpoints.serverFromString(reactor, "ssl:15987:privateKey=cert/privkey.pem:certKey=cert/server.pem").listen(server.Site(Server()))
logger.info("Running Cryptonite Server...")
reactor.run()

#serverFromString(
#       reactor, b"ssl:443:privateKey=key.pem:certKey=crt.pem")


# endpoints.serverFromString(reactor, "tcp:15987")
# endpoints.SSL4ServerEndpoint(reactor, 15987)
# server.Site(Counter())
# endpoints.TCP4ServerEndpoint(reactor, 15987)
