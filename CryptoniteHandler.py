
import json

# Cryptonite imports
import DataAccess
import logger
import utils
from model import User
from model import Session
import time


class CryptoniteHandler:
    
    def __init__(self):
        self.context = logger.RequestContext()  # Stats & logging object
        
        self.request_type = None    # Type of request (li, gk, nk, dk, cuwa)
        self.user = None            # User obtained from session key
        self.request_json = None    # Request body

    def handle_request(self, request, postData):
        """ Returns json.dumps(...) string. """

        # Try to parse JSON request
        # -------------------------
        try:
            self.request_json = json.loads(postData)
        except ValueError as e:
            logger.error("json parse error", e)
            return json.dumps({"errm": "error with json", "errn": 2})

        # Validate data
        # -------------
        # TODO: Finish this shit
        if utils.validate_json(self.request_json) is False:
            # Error was already logged
            return json.dumps({"errm": "error with validation", "errn": 3})

        # Handle request
        # --------------

        # Get request type
        self.request_type = str(self.request_json["rty"])
        self.context.request_type = self.request_type    # For logging and statistics purposes

        # Check session key (all types except login)
        if self.request_type != "li":
            session = Session.from_session_key(self.request_json["sek"])
            if session is None:
                return json.dumps({"errm": "Session key invalid", "errn": 4})
            self.user = session.user

        # Handle each type
        try:
            # LOGIN
            if self.request_type == "li":
                print("Request Type = Login")                
                response = self.handle_login()

            # GET KEYS
            elif self.request_type == "gk":
                print('Request Type = Get Keys')
                response = self.handle_getkeys()
            
            # NEW KEYS     
            elif self.request_type == "nk":
                print('Request Type = New Keys') 
                response = self.handle_newkeys()
            
            # DELETE KEYS
            elif self.request_type == "dk":
                print('Request Type = Delete Keys')
                response = self.handle_deletekeys()
                
            # Change User With Access
            elif self.request_type == "cuwa":
                response = self.handle_changeuserswithaccess()
                print("Request type = CUWA")

            else:
                logger.info("Unknown request type! - " + str(self.request_type))
                return json.dumps({"errm": "error with json", "errn": 2})
                    
        # For json errors
        except KeyError as e:
            logger.error("Error with JSON.", e)
            return json.dumps({"errm": "error with json", "errn": 2})
        # Catch-all just in case
        except Exception as e:
            logger.error("Generic error in handler.", e)
            return json.dumps({"errm": "generic error in handler", "errn": 78})
    
        return response










    def handle_login(self):
        """ Returns JSON response for login call. """  

        username = self.request_json["lid"]["un"]
        password = self.request_json["lid"]["p"]

        #time.sleep(2)

        # Get user from db
        user = User.from_login(username, password)
        if user is None:
            logger.info("Login failed.")
            return json.dumps({"errm": "Login not successfull", "errn": 0})

        # Create session
        session = Session(user)

        # Cache the session in memcached
        result = session.cache_session()
        if result is False:
            logger.info("Caching session failed")
            return json.dumps({"errm": "Login not successfull", "errn": 1})

        # Construct return Json from User and Session                    
        return json.dumps({'sek': session.session_key,
                           'npsk': None,
                           'ud': {'u1': user.u1, 'u2': user.u2, 'u3': user.u3, 'u4': user.u4, 'u5': user.u5}
                           })




    def handle_getkeys(self):
        """ Returns JSON object filled with keys. """
        
        # Get the keys
        object_type = self.request_json["ot"]
        object_data = self.request_json["objs"]
        
        object_list = DataAccess.getKeys(self.user, object_data, object_type)
        
        # Convert objects in list to Json representation -- this will have to be moved to Object class or smth
        jsonObjectList = []        
        for obj in object_list:
            if obj == None:
                jsonObj = {}
            else:         
                jsonObj = {'ooid': obj[0][1], 'k': obj[0][2], 'ot': obj[0][6], 'io': obj[0][5], 'od': {'ic': obj[0][3], 'cw': obj[0][4]}}
            #jsonObj = {'ooid': "Bla", 'k': obj[0][2], 'ot': obj[0][6], 'io': obj[0][5], 'od': {'ic': obj[0][3], 'cw': "Bla"}}
            jsonObjectList.append(jsonObj)         
                
        return json.dumps(jsonObjectList)            



    
    def handle_deletekeys(self):
        """ Returns json OK or ERROR message """

        # Delete those keys!
        objectData = self.request_json["objs"]
        returnJson = DataAccess.deleteKeys(self.user, objectData)
        
        return returnJson




    def handle_newkeys(self):

        # Save new keys
        objectData = self.request_json["objs"]
        jsonReturn = DataAccess.saveKeys(self.user, objectData)
        
        return jsonReturn
        # return json.dumps(self.dataCache.saveKeys(userId, jsonData['objs']))




    def handle_changeuserswithaccess(self):

        # Change access for users
        object_data = self.request_json["objs"]
        json_return = DataAccess.change_access(self.user, object_data, self.request_json["ot"])

        return json_return
