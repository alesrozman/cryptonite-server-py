from __future__ import with_statement
from contextlib import contextmanager
import traceback, sys

# import psycopg2
import psycopg2.pool


connectionPool = psycopg2.pool.SimpleConnectionPool(1, 20, "dbname='crypt-o-nite' user='postgres' host='localhost' password='a0ummz8fk'")


@contextmanager
def getcursor():
    con = connectionPool.getconn()
    try:
        yield con.cursor()
        con.commit()
    except:
        con.rollback()
        traceback.print_exc(file=sys.stdout)
        raise
    finally:
        connectionPool.putconn(con)
