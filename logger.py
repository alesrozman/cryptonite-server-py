from twisted.python import log

import sys


def init():
    # log.startLogging(open('/home/bob/cryptolog/log.log', 'a'), setStdout=False)
    # log.startLoggingWithObserver(log.startLogging(open('/home/bob/cryptolog/log.log', 'a')), False)
    log.startLogging(sys.stdout)
    

def info(msg):
    log.msg(msg)
    # print msg


def error(msg, exception=None):
    log.err(exception, msg)
    # print msg
    # Print exception to stdout
    

class RequestContext:

    def __init__(self):
        self.request_type = None
        self.start_time = None
        self.end_time = None

    def to_json(self):
        return {"request_type": str(self.request_type),
                "duration": str(self.end_time - self.start_time)}
