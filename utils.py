__author__ = 'bob'

import hashlib
import base64
import random

import logger


def create_hash(string_to_hash, salt):
    return hashlib.sha512(base64.b64decode(salt, '+-') + bytes(str(string_to_hash))).hexdigest()


def generate_salt():
    salt = []
    rand = random.SystemRandom()
    for i in range(64):
        salt.append(rand.randrange(0, 255, 1))

    return str(base64.b64encode(bytearray(salt), b'+-'))


def generate_session_key():
    session_key = []
    rand = random.SystemRandom()
    for i in range(150):
        session_key.append(rand.randrange(0, 255, 1))

    return base64.b64encode(bytearray(session_key), b'+-')


def validate_json(json):

    try:
        # Validate request type
        rt = str(json["rty"])
        if rt != "li" and rt != "gk" and rt != "nk" and rt != "dk" and rt != "cuwa":
            raise Exception("Wrong rty")

        # Validate login type
        # {"rty":"li","lid":{"un":"goran","p":"a","psk":null}}
        if rt == "li":
            assert len(json) == 2
            assert len(json["lid"]) == 3
            assert json["lid"]["un"]
            assert json["lid"]["p"]
            json["lid"]["psk"]  # optional value

    except Exception as e:
        logger.error("Validation error!", e)
        return False

    return True
