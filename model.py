
from DBConnectionPool import getcursor

import memcache

# Cryptonite imports
import utils
import logger


# Initialize memcached
mc = memcache.Client(['127.0.0.1:11211'], debug=0)


class User:

    def __init__(self, _id=0, username=None, password=None, salt=None, u1=None, u2=None, u3=None, u4=None, u5=None):
        self.id = _id
        self.username = username
        self.password = password
        self.salt = salt
        self.u1 = u1
        self.u2 = u2
        self.u3 = u3
        self.u4 = u4
        self.u5 = u5

    @classmethod
    def from_login(cls, username, password):
        """
        Cretes a User object by getting it from database with username/password parameters.
        :param username:
        :param password:
        :return: User object or None
        """

        # Basic validation
        if username is None or password is None:
            return None

        with getcursor() as cur:

            # Get user from db via username
            cur.execute("""SELECT id, username, password, salt, u1, u2, u3, u4, u5
                           FROM users
                           WHERE username = %s;""", (username,))
            db_user = cur.fetchall()

            if len(db_user) == 0:
                logger.error("No user found with username: " + str(username))
                return None
            elif len(db_user) > 1:
                logger.error("Multiple users found with username: " + str(username))
                return None

            # Create a user object
            user = User(db_user[0][0], db_user[0][1], db_user[0][2], db_user[0][3], db_user[0][4], db_user[0][5],
                        db_user[0][6], db_user[0][7], db_user[0][8])

            # Check password
            if user.password != utils.create_hash(password, user.salt):
                logger.info("Wrong password for user " + str(user.username))
                return None

            return user

    @classmethod
    def from_service(cls, service):

        # Basic validation
        if service is None:
            return None

        with getcursor() as cur:

            # Search for user in db
            cur.execute("""SELECT id, username, salt, u1, u2, u3, u4, u5
                           FROM users
                           WHERE u1 = %(service)s OR u2 = %(service)s OR u3 = %(service)s
                              OR u4 = %(service)s OR u5 = %(service)s;""",
                        {"service": service})
            db_user = cur.fetchall()

            if len(db_user) == 0:
                logger.error("No user found with service: " + str(service))
                return None
            elif len(db_user) > 1:
                logger.error("Multiple users found with service: " + str(service))
                return None

            user = User(db_user[0][0], db_user[0][1], None, db_user[0][2], db_user[0][3], db_user[0][4], db_user[0][5],
                        db_user[0][6], db_user[0][7])

            return user

    def __repr__(self):
        return "USER: id(" + str(self.id) + ") username(" + str(self.username) + ") salt(" + str(self.salt) + \
            ") u1(" + str(self.u1) + ") u2(" + str(self.u2) + ") u3(" + str(self.u3) + ") u4(" + str(self.u4) + \
            ") u5(" + str(self.u5) + ")"


class Object:

    def __init__(self, original_object_id=None, key=None, init_chain=None,
                 checkword=None, is_owner=False, object_type=0):
        self.original_object_id = original_object_id
        self.key = key
        self.init_chain = init_chain
        self.checkword = checkword
        self.is_owner = is_owner
        self.object_type = object_type

    def __repr__(self):
        return "OBJECT: objectType(" + str(self.object_type) + ") ooid(" + str(self.original_object_id) + ") key(" + \
            str(self.key) + ") initChain(" + str(self.init_chain) + ") checkword(" + str(self.checkword) + ") isOwner(" + \
            str(self.is_owner) + ")"

    def to_json(self):
        return {"ooid": self.original_object_id,
                "k": self.key,
                "ot": self.object_type,
                "io": self.is_owner,
                "od": {"ic": self.init_chain, "cw": self.checkword}}


class Session:

    def __init__(self, user):
        self.user = user
        self.session_key = utils.generate_session_key()

    @classmethod
    def from_session_key(cls, session_key):

        # Check in memcached
        saved_session = mc.get(str(session_key))
        if not saved_session:
            return None
        else:
            assert saved_session.user is not None
            return saved_session

    @classmethod
    def from_new_session(cls):
        pass

    def cache_session(self):
        # Basic validation
        if self.user is None or self.session_key is None:
            return False

        # Put session in memcached
        try:
            key = str(self.session_key)
            mc.set(key, self, 3600)
        except Exception as e:
            logger.error("Error while setting in memcahced", e)
            return False

        return True

    def __repr__(self):
        return "Session: User(" + str(self.user) + ") SessionKey(" + str(self.session_key) + ")"
