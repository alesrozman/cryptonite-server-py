from DBConnectionPool import getcursor

import hashlib
import base64
import sys
import traceback
import json

from User import User


def getUser(username, password):
    """ Searches database for a user with supplied username/password. 
    @return: User object or None if credentials were incorrect
    @param username: User's username
    @param password: User's password
    @raise stuff: No exception handling
    """
    
    user = None
        
    with getcursor() as cur:
        # change it because of hashing
        cur.execute("""SELECT id, username, password, u1, u2, u3, u4, u5, salt FROM users WHERE username=%s;""", (username,))
        user_rs = cur.fetchall()                      

        if len(user_rs) == 0:
            print("User error (getUserFromDb): no user with username = ", username, " and password = ", password)
            return None
        
        elif len(user_rs) > 1:
            print("User error (getUserFromDb): more then one user with username = ", username, " and password = ", password)
            return None
        
        # check hashed password (with salt)
        password_hash = hashlib.sha512(base64.b64decode(user_rs[0][8], '+-') + bytes(str(password))).hexdigest()
        if user_rs[0][2] != password_hash:
            return None
                
        user = User()
        user.id = user_rs[0][0]
        user.username = user_rs[0][1]
        user.u1 = user_rs[0][3]
        user.u2 = user_rs[0][4]
        user.u3 = user_rs[0][5]
        user.u4 = user_rs[0][6]
        user.u5 = user_rs[0][7]
        user.salt = user_rs[0][8]
      
    return user

                  
        
def getKeys(user, objectData, objectType):
    """ Returns objectList, an array of objects, which are a db line tuples"""
         
    tableName = str(user.id)  # hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(user.id))).hexdigest()
    
    objectList = []

    for theObject in objectData:
        
        try:                      
            ooid = theObject["ooid"]
            ooid_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(ooid))).hexdigest()
              
            # There is no checkword
            if len(theObject) == 1:
                # Search the db with ooid
                obj = getObjectByOoid(tableName, ooid_hash, objectType)
                if obj is not None:
                    objectList.append(obj)
                    continue
                
                # If object was not found, log it
                print "Object not found. ooid:", str(ooid)
                # Insert None so we preserve length and positioning in array
                objectList.append(None)
            
            # checkword is present
            elif len(theObject) == 2:
                # Search first by ooid
                obj = getObjectByOoid(tableName, ooid_hash, objectType) # TODO: Ce najdes 2 ali vec po ooid, isces se po ooid in cw                               
                if obj is not None:

                    # Special case for gdrive files
                    if objectType == 6:
                        if obj[0][2] == "-1":
                            # key is -1 meaning we must find this checkword in gmail attachments
                            k_checkword = theObject["cw"]
                            k_checkword_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(k_checkword))).hexdigest()
                            k_obj = getObjectByCheckword2(tableName, k_checkword_hash, 4)
                            if k_obj is not None:
                                # we found the right attachment, now we copy key, ic and cw
                                updateObjectWithKeyData(tableName, obj[0][1], k_obj[0][2], k_obj[0][3], k_checkword_hash, objectType)
                                # now we get the updated record
                                obj = getObjectByOoid(tableName, ooid_hash, objectType)
                    # ---- end special case -------

                    objectList.append(obj)
                    continue
                
                # Next we search by checkword
                checkword = theObject["cw"]
                checkword_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(checkword))).hexdigest()
                
                # -- Here we handle special case of hotmail group mails
                if objectType == 2:                                      
                    # Check if ooid starts with cv and is exact lenght
                    if ooid[:2] == "cv":                        
                        # Now we search by checkword
                        obj = getObjectByCheckwordOnly(tableName, checkword_hash, objectType)
                        if obj is not None:
                            # Insert a new record with this 'cv' ooid
                            insertObject(tableName, ooid_hash, obj[0][2], obj[0][3], checkword_hash, True, objectType)
                            # Query the new record
                            obj2 = getObjectByOoid(tableName, ooid_hash, objectType)
                            objectList.append(obj2)
                            continue                
                # -- End special case
                
                obj = getObjectByCheckword(tableName, checkword_hash, objectType)
                if obj is not None:
                    objectList.append(obj)
                    # If we found object by checkword, we must update its ooid
                    updateObjectOoid(tableName, ooid_hash, obj[0][0], checkword_hash, objectType)
                    continue
                
                # If object was not found, log it
                print "Object not found. ooid:", str(ooid), " cw:", str(checkword)
                # Insert None so we preserve length and positioning in array
                objectList.append(None)
    
        except:
            # Log exception and continue with next element
            print "Exception in getKeys for loop."            
            print(sys.exc_info()[0])
            return None
    
    return objectList
          
     
def deleteKeys(user, objectData):
    
    try:
        tableName = str(user.id)
        
        for theObject in objectData:
            # Prepare data
            ooid = theObject["ooid"]
            ooid_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(ooid))).hexdigest()
            objectType = theObject["ot"]
            
            # TODO Maybe :)
            # If you delete a conversation in Google, we must delete all asociated email keys.
            # This means we delete all records with the same objectType, checkword, init chain, key
            
            # Search for the object in database
            
            # Delete object from db
            deleteObjectByOoid(tableName, ooid_hash, objectType)
           
        return json.dumps({"errm": "OK"})
    except:
        traceback.print_exc(file=sys.stdout)
        return json.dumps({"errm" : "Exception in deleteKeys", "errn" : 8})
        


def saveKeys(user, objectData):
    tableName = str(user.id)  # hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(user.id))).hexdigest()
    
    for theObject in objectData:        
        
        # Prepare data - request (received json) variables have req_ prefix
        # -----------------------------------------------------------------

        # OOID and it's hash
        req_ooid = theObject["obj"]["ooid"]
        if req_ooid == "-1":
            req_ooid_hash = "-1"
        else:
            req_ooid_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(req_ooid))).hexdigest()

        # KEY
        req_key = theObject["obj"]["k"]

        # OBJECT TYPE
        req_object_type = theObject["obj"]["ot"]

        # INIT CHAIN - is optional
        req_init_chain = None
        try:
            req_init_chain = theObject["obj"]["od"]["ic"]
        except:
            pass

        # CHECKWORD - is optional
        req_checkword = None
        req_checkword_hash = None
        try:
            req_checkword = theObject["obj"]["od"]["cw"]
            if req_checkword != None:
                req_checkword_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(req_checkword))).hexdigest()
        except:
            pass

        # DRAFT ID - is optional
        req_draft_id = None
        req_draft_hash = None
        try:
            req_draft_id = theObject["obj"]["od"]["did"]
            req_draft_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(req_draft_id))).hexdigest()
        except:
            pass

        # ATTACHMENTS - are optional
        req_atts = None
        try:
            req_atts = theObject["obj"]["od"]["att"]
        except:
            pass

        # USERS WITH ACCESS - is optional
        req_uwas = None
        try:
            req_uwas = theObject["uwa"]
        except:
            pass

        # --- Data prepare complete ----------------------------------------

        # Case for GDOCS
        # --------------

        if req_object_type == 0:
            # No special logic here, we just insert the object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

        # Case for GDRIVE FILES
        # ---------------------

        elif req_object_type == 6:
            # No special logic here, we just insert the object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

        # Case for ONEDRIVE FILES
        # -----------------------

        elif req_object_type == 7:
            # No special logic here, we just insert the object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

        # Case for GMAIL ATTACHMENT
        # -------------------------

        elif req_object_type == 4:
            # Normal insert object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

            # Insert into special attachments table, cw is not hashed
            insertAttDraft(req_ooid_hash, None, req_checkword)

            # Insert into GDRIVE FILE to facilitate "move to drive" action
            insertObject(tableName, "-1", req_key, req_init_chain, req_checkword_hash, True, 6)

        # Case for HOTMAIL ATTACHMENT
        # ---------------------------

        elif req_object_type == 5:
            # Normal insert object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

            # Insert into special attachments table, both ooid and cw are not hashed
            insertAttDraft(req_ooid, None, req_checkword)

            # -- NEW HOTMAIL --
            insertAttDraft(req_ooid_hash, None, req_checkword)
            # -- NEW HOTMAIL end --

            # Insert into ONEDRIVE FILE to facilitate "move to drive" action
            insertObject(tableName, "-1", req_key, req_init_chain, req_checkword_hash, True, 7)

        # Case for GMAIL
        # --------------

        elif req_object_type == 1:
            # Insert into object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

            # If draft_id is present, delete that draft and update attachments table
            if req_draft_id is not None:
                deleteObjectByOoid(tableName, req_draft_hash, req_object_type)
                updateAttDraft(req_ooid_hash, req_draft_hash)  # TODO: add user id to this table for quicker querys

            # If attachment reference is present, update attachments table
            if req_atts is not None:
                for attachment in req_atts:
                    att_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(attachment))).hexdigest()
                    updateAttDraftWithDraftId(att_hash, req_ooid_hash)

            # If UWA is present, insert mail and attachments to recipient's tables
            if len(req_uwas) > 0:

                # First gather all connected attachments
                # --------------------------------------

                # Search for GMAIL attachments in linking table
                # print "This is send GMAIL - starting to make attachments list..."
                att_ret = getAttDraftByDraftId(req_ooid_hash)
                if att_ret is not None:
                    # att_ret is (att_id, cw)
                    att_key = []
                    att_ic = []
                    att_cw = []
                    for att_ret_obj in att_ret:
                        # print "Getting attachments from att_draft for sending ... uwa"
                        obj = getObjectByOoid(tableName, att_ret_obj[0], 4)
                        if obj is not None:
                            att_key.append(obj[0][2])
                            att_ic.append(obj[0][3])
                            att_cw.append(att_ret_obj[1])
                        else:
                            pass
                            # This is fucked up ...

                # Attachments data is now in att_key, att_ic, att_cw

                # Insert mails and attachments for all recipients
                # -----------------------------------------------

                for service_name in req_uwas:
                    # Determine objectType from serviceName
                    other_object_type = determineObjectType(service_name)
                    if other_object_type is None:
                        print "determineObjectType was None for", service_name
                        continue

                    # Get the user who has this service (email)
                    other_user = getUserByService(service_name)
                    if other_user is None:
                        # log error
                        print "getUserByService was None for service", service_name
                        continue

                    # Prepare data for inserting
                    other_table_name = str(other_user.id)
                    other_checkword_hash = None
                    if req_checkword is not None:
                        other_checkword_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(req_checkword))).hexdigest()

                    # Insert object into his table
                    insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, other_object_type)

                    # TODO: workaround for new hotmail
                    # Insert another for new hotmail
                    if other_object_type == 2:
                        insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, 8)
                    # --------------------------------

                    # Insert attachments
                    if att_ret is not None:
                        # Based on where the mail is going, we set object_type
                        if other_object_type == 1:
                            att_ot = 4
                            att_drive_ot = 6
                        elif other_object_type == 2:
                            att_ot = 5
                            att_drive_ot = 7

                        for i in range(0, len(att_ret)):
                            att_cw_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(att_cw[i]))).hexdigest()
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_ot)

                            # Insert into Drive to facilitate "move to drive" functionality
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_drive_ot)

                # TODO: delete from att_draft table

        # Case for HOTMAIL
        # ----------------

        elif req_object_type == 2:   # or req_object_type == 8:

            # TODO: objtype 8 is not tested

            # Insert into object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

            # If draft_id is present, delete that draft and update attachments table
            if req_draft_id is not None:
                deleteObjectByOoid(tableName, req_draft_hash, req_object_type)
                if req_ooid != "-1":    # New hotmail exception. Last ot:8 request will have ooid -1.
                    updateAttDraft(req_ooid_hash, req_draft_hash)  # TODO: not sure this is needed in hotmail

            # NEW HOTMAIL will have att in draft
            # if req_atts is not None and req_draft_id is not None:   # condition for new hotmail
            #    for attachment in req_atts:
            #        att_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(attachment))).hexdigest()
            #       updateAttDraftWithDraftId(att_hash, req_ooid_hash)
            # -----------

            # If UWA is present, insert mail and attachments to other people's tables
            if len(req_uwas) > 0:

                # First create a list of all attachments
                # --------------------------------------

                att_key = []
                att_ic = []
                att_cw = []

                # In case of hotmail, we receive a list of attachments in atts
                if req_atts is not None:
                    for att in req_atts:
                        # Search for HOTMAIL attachments in linking table to get cw
                        obj = get_attdraft_by_attachment_id(att)
                        if obj is not None:
                            att_cw.append(obj[0][1])
                        # Search for HOTMAIL attachment in objects to get key, ic
                        att_hash_temp = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(att))).hexdigest()
                        obj = getObjectByOoid(tableName, att_hash_temp, 5)
                        if obj is not None:
                            att_ic.append(obj[0][3])
                            att_key.append(obj[0][2])

                # If atts are not present, means that it's new hotmail and atts were sent with previous draft
                """else:
                    # Search for attachments in linking table
                    # att_ret = getAttDraftByDraftId(req_ooid_hash)

                    # TODO: check if draft id is present
                    att_ret = getAttDraftByDraftId(req_draft_hash)

                    # Strange case fix TODO: should be caught elsewhere WTF
                    #if req_ooid_hash == "-1":
                    #    att_ret = None
                    # -----------------

                    if att_ret is not None:
                        print "NEW HOTMAIL: Found att in linking table."
                        # att_ret is (att_id, cw)
                        att_key = []
                        att_ic = []
                        att_cw = []
                        for att_ret_obj in att_ret:
                            # print "Getting attachments from att_draft for sending ... uwa"
                            obj = getObjectByOoid(tableName, att_ret_obj[0], 5)
                            if obj is not None:
                                att_key.append(obj[0][2])
                                att_ic.append(obj[0][3])
                                att_cw.append(att_ret_obj[1])
                            else:
                                pass
                                # This is fucked up ...
                """

                # Attachment data is now in att_key, att_ic, att_cw

                # Insert mails and attachments for all recipients
                # -----------------------------------------------

                for service_name in req_uwas:
                    # Determine objectType from serviceName
                    other_object_type = determineObjectType(service_name)
                    if other_object_type is None:
                        print "determineObjectType was None for", service_name
                        continue

                    # Get the user who has this service (email)
                    other_user = getUserByService(service_name)
                    if other_user is None:
                        # log error
                        print "getUserByService was None for service", service_name
                        continue

                    # Prepare data for inserting
                    other_table_name = str(other_user.id)
                    other_checkword_hash = None
                    if req_checkword is not None:
                        other_checkword_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(req_checkword))).hexdigest()

                    # Insert mail object into his table
                    insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, other_object_type)

                    # TODO: workaround for new hotmail
                    # Insert another for new hotmail
                    if other_object_type == 2:
                        insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, 8)
                    # --------------------------------

                    # Insert attachments
                    if req_atts is not None:

                        # Determine attachment type based on where it's going
                        if other_object_type == 1:
                            att_ot = 4
                            att_drive_ot = 6
                        elif other_object_type == 2:
                            att_ot = 5
                            att_drive_ot = 7

                        for i in range(0, len(req_atts)):
                            # Set ooid to -1 for hotmail attachments so they will work in sent folder
                            att_hash_temp2 = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(req_atts[i]))).hexdigest()
                            updateObjectOoidForHotmailAttachment(tableName, att_hash_temp2)  # TODO: dvomim da to dela

                            # Insert into recipient table
                            att_cw_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(att_cw[i]))).hexdigest()
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_ot)

                            # Insert into Drive to facilitate "move to drive" functionality
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_drive_ot)

                    # Insert attachments for new hotmail
                    #if req_object_type == 8 and att_ret is not None:
                    #    # Based on where the mail is going, we set object_type
                    #    if other_object_type == 1:
                    #        att_ot = 4
                    #        att_drive_ot = 6
                    #    elif other_object_type == 2:
                    #        att_ot = 5
                    #        att_drive_ot = 7

                    #    for i in range(0, len(att_ret)):
                    #        att_cw_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(att_cw[i]))).hexdigest()
                    #        insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_ot)

                    #        # Insert into Drive to facilitate "move to drive" functionality
                    #        insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_drive_ot)

        # Case for HOTMAIL NEW
        # --------------------
        elif req_object_type == 8:

            # Insert into object
            insertObject(tableName, req_ooid_hash, req_key, req_init_chain, req_checkword_hash, True, req_object_type)

            # If draft_id is present, delete that draft and update attachments table
            if req_draft_id is not None:
                deleteObjectByOoid(tableName, req_draft_hash, req_object_type)
                if req_ooid != "-1":    # New hotmail exception. Last ot:8 request will have ooid -1.
                    updateAttDraft(req_ooid_hash, req_draft_hash)  # TODO: not sure this is needed in hotmail

            # Attachment ids are present, so we update att_draft table
            if req_atts is not None:
                for attachment in req_atts:
                    att_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(attachment))).hexdigest()
                    updateAttDraftWithDraftId(att_hash, req_ooid_hash)

            # If UWA is present, insert mail and attachments to other people's tables
            if len(req_uwas) > 0:

                # First create a list of all attachments
                # --------------------------------------

                att_key = []
                att_ic = []
                att_cw = []

                # Search for attachments in linking table based on draft_id
                att_ret = None
                if req_draft_id is not None:
                    att_ret = getAttDraftByDraftId(req_draft_hash)

                if att_ret is not None:
                    # att_ret is (att_id, cw)
                    att_key = []
                    att_ic = []
                    att_cw = []
                    for att_ret_obj in att_ret:
                        obj = getObjectByOoid(tableName, att_ret_obj[0], 5)
                        if obj is not None:
                            att_key.append(obj[0][2])
                            att_ic.append(obj[0][3])
                            att_cw.append(att_ret_obj[1])
                        else:
                            pass
                            # todo: shuld not happen, buuut we know how that works :)

                # Attachment data is now in att_key, att_ic, att_cw

                # Insert mails and attachments for all recipients
                # -----------------------------------------------

                for service_name in req_uwas:

                    # Determine objectType from serviceName
                    other_object_type = determineObjectType(service_name)
                    if other_object_type is None:
                        print "determineObjectType was None for", service_name
                        continue

                    # Get the user who has this service (email)
                    other_user = getUserByService(service_name)
                    if other_user is None:
                        # log error
                        print "getUserByService was None for service", service_name
                        continue

                    # Prepare data for inserting
                    other_table_name = str(other_user.id)
                    other_checkword_hash = None
                    if req_checkword is not None:
                        other_checkword_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(req_checkword))).hexdigest()

                    # Insert mail object into his table
                    insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, other_object_type)

                    # TODO: workaround for new hotmail
                    # Insert another for new hotmail
                    if other_object_type == 2:
                        insertObject(other_table_name, "-1", req_key, req_init_chain, other_checkword_hash, True, 8)
                    # --------------------------------

                    # Insert attachments into recipient's table
                    if att_ret is not None:
                        # Based on where the mail is going, we set object_type
                        if other_object_type == 1:
                            att_ot = 4
                            att_drive_ot = 6
                        elif other_object_type == 2:
                            att_ot = 5
                            att_drive_ot = 7

                        for i in range(0, len(att_ret)):
                            att_cw_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(att_cw[i]))).hexdigest()
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_ot)

                            # Insert into Drive to facilitate "move to drive" functionality
                            insertObject(other_table_name, "-1", att_key[i], att_ic[i], att_cw_hash, True, att_drive_ot)

        # end case for hotmail new

    # end of for loop - but at the moment it's always one item, one iteration
        
    return json.dumps({"errm": "OK"})


def change_access(user, object_data, req_object_type):

    """
    11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] POST --------------------------
2015-07-11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Headers({'content-length': ['331'], 'accept-language': ['en-US,en;q=0.5'], 'accept-encoding': ['gzip, deflate'], 'connection': ['keep-alive'], 'accept': ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'], 'user-agent': ['Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'], 'host': ['213.157.239.72:15987'], 'content-type': ['application/json']})
2015-07-11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Received: {"rty":"cuwa","sek":"lmAe9Vb1+dTwkncbl3bYoCMrgvwfVcKUT3KnqI6-p1+NV7XrfqJLUi6PKu99tdSBXrbW78+E+-fAs8LFGP2zRlPGSBTEl23h8fRGbo6TB-ANg4XuBkTQjbvtJFYW8nla2J3cRe-Q95PJ5Jxqd7oYDoH8t0hXyxnvqiyVsL+oAjVm47ngQo1IhAfWP5Zp8rWnP9vZjVP8","objs":[{"ooid":"10fyQJ0h2dZ9NMYwi1acTcTLOLERd4ZptGOOpAeFYBjQ","t":"r","uwa":["ales.rozman.si@gmail.com"]}]}
2015-07-11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] rty = cuwa
2015-07-11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Sending response (14): {"errm": "OK"}
2015-07-11 20:49:44+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] 92.37.87.150 - - [11/Jul/2015:18:49:43 +0000] "POST / HTTP/1.1" 200 14 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0"
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] POST --------------------------
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Headers({'content-length': ['331'], 'accept-language': ['en-US,en;q=0.5'], 'accept-encoding': ['gzip, deflate'], 'connection': ['keep-alive'], 'accept': ['text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'], 'user-agent': ['Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'], 'host': ['213.157.239.72:15987'], 'content-type': ['application/json']})
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Received:
{
"rty":"cuwa",
"sek":"lmAe9Vb1+dTwkncbl3bYoCMrgvwfVcKUT3KnqI6-p1+NV7XrfqJLUi6PKu99tdSBXrbW78+E+-fAs8LFGP2zRlPGSBTEl23h8fRGbo6TB-ANg4XuBkTQjbvtJFYW8nla2J3cRe-Q95PJ5Jxqd7oYDoH8t0hXyxnvqiyVsL+oAjVm47ngQo1IhAfWP5Zp8rWnP9vZjVP8",
"ot":"6"  -- novo
"objs":[{
    "ooid":"10fyQJ0h2dZ9NMYwi1acTcTLOLERd4ZptGOOpAeFYBjQ",
    "t":"a",
    "uwa":["ales.rozman.si@gmail.com"]
}]
}
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] rty = cuwa
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] Sending response (14): {"errm": "OK"}
2015-07-11 20:49:48+0200 [HTTPChannel (TLSMemoryBIOProtocol),26,92.37.87.150] 92.37.87.150 - - [11/Jul/2015:18:49:47 +0000] "POST / HTTP/1.1" 200 14 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0"

"ot":"6"

    """

    table_name = str(user.id)

    # object_data = json["objs"] means it is array

    for req_object in object_data:

        # Prepare data
        req_ooid = req_object["ooid"]
        req_ooid_hash = hashlib.sha512(base64.b64decode(user.salt, '+-') + bytes(str(req_ooid))).hexdigest()
        req_type = req_object["t"]
        req_uwa = req_object["uwa"]

        # Request came from GDRIVE, it could be a gdoc or gdrive file
        # -----------------------------------------------------------
        if req_object_type == 6 or req_object_type == 0:    # Added 0 (GDOC) just in case

            # First we check if it is a GDOC
            obj = getObjectByOoid(table_name, req_ooid_hash, 0)

            if obj is not None:
                # Prepare data for inserting GDOC
                print "CUWA: You are sharing a GDOC."
                object_type = 0
                object_key = obj[0][2]
                object_ic = obj[0][3]   # This should be null for GDOC
            else:
                # Object is not GDOC, meaning it is GDRIVE file
                # -- This should be done differently before onedrive is added
                # -- Maybe some indication that request was from gdrive, onedrive, etc ...

                obj2 = getObjectByOoid(table_name, req_ooid_hash, 6)

                if obj2 is not None:
                    print "CUWA: You are sharing a GDRIVE file."
                    # Prepare data for inserting GDRIVE file
                    object_type = 6
                    object_key = obj2[0][2]
                    object_ic = obj2[0][3]

                else:
                    # Object was not found in GDOC or GDRIVE
                    # We assume it was not opened yet, so we insert a GDRIVE in other user with just ooid
                    #   and we make a record in special table for keeping track of such things.
                    #
                    # TODO: How do we know that the file is actualy cryptonite? Can we know?

                    print "CUWA: Shared object no found, assuming unopened GDRIVE file."
                    # Prepare data for unopened GDRIVE file
                    object_type = 6
                    object_key = None
                    object_ic = None

        # Request came from ONEDRIVE
        # --------------------------
        elif req_object_type == 7:

            # Search for record in ONEDRIVEFILE
            obj = getObjectByOoid(table_name, req_ooid_hash, 7)

            if obj is not None:
                print "CUWA: You are sharing a ONEDRIVE file."
                # Prepare data for inserting ONEDRIVE file
                object_type = 7
                object_key = obj[0][2]
                object_ic = obj[0][3]

            else:
                # Object was not found in ONEDRIVEFILE
                # We assume it was not opened yet, so we insert a ONEDRIVEFILE in other user with just ooid
                #   and we make a record in special table for keeping track of such things.

                print "CUWA: Shared object no found, assuming unopened ONEDRIVE file."
                # Prepare data for unopened ONEDRIVE file
                object_type = 7
                object_key = None
                object_ic = None

        else:
            # Wrong or unsupported object type?
            print "CUWA: Unsupported object type: " + str(req_object_type)
            continue

        # We have the data prepared, let's insert into other user's tables

        for service_name in req_uwa:

            print "CUWA: Inserting/Deleting for user " + str(service_name)

            # Get the user who has this service (email)
            other_user = getUserByService(service_name)
            if other_user is None:
                # log error
                print "getUserByService was None for service ", service_name
                continue

            other_table_name = str(other_user.id)
            other_ooid_hash = hashlib.sha512(base64.b64decode(other_user.salt, '+-') + bytes(str(req_ooid))).hexdigest()

            # Adding access
            if req_type == "a":
                insertObject(other_table_name, other_ooid_hash, object_key, object_ic, None, False, object_type)

            # Removing access
            elif req_type == "r":
                deleteObjectByOoid(other_table_name, other_ooid_hash, object_type)

            else:
                # error which should be caught by some kind of input validation
                print "CUWA error: type is not 'a' or 'r'"

        # End for through services

    # End for through objs

    return json.dumps({"errm": "OK"})

            
# ----------------
# Helper functions
# ----------------


def getObjectByOoid(tableName, ooid_hash, objectType):    
    with getcursor() as cur:
        cur.execute("""SELECT * FROM objects""" + tableName + """ WHERE original_object_id=%s AND id_object_types=%s;""", (ooid_hash, objectType))        
        objdb = cur.fetchall()
        
        if len(objdb) == 0:
            print "getObjectByOoid not found"
            return None
        
        print "getObjectByOoid:", str(objdb)        
        return objdb
    
"""def getObjectByOoidAndCheckword(tableName, ooid_hash, checkword_hash, objectType):
    with getcursor() as cur:
        cur.execute()"""


def getObjectByCheckword(tableName, checkword_hash, objectType):
    with getcursor() as cur:
        cur.execute("""SELECT * FROM objects""" + tableName + """ WHERE original_object_id='-1' AND check_word=%s AND id_object_types=%s LIMIT 1;""", (checkword_hash, objectType))
        objdb = cur.fetchall()
        
        if len(objdb) == 0:
            print "getObjectByCheckword not found"
            return None
              
        print "getObjectByCheckword:", str(objdb)        
        return objdb

def getObjectByCheckword2(tableName, checkword_hash, objectType): # TODO: !!!!!
    with getcursor() as cur:
        cur.execute("""SELECT * FROM objects""" + tableName + """ WHERE check_word=%s AND id_object_types=%s LIMIT 1;""", (checkword_hash, objectType))
        objdb = cur.fetchall()

        if len(objdb) == 0:
            print "getObjectByCheckword not found"
            return None

        print "getObjectByCheckword:", str(objdb)
        return objdb

def getObjectByCheckwordOnly(tableName, checkword_hash, objectType):
    with getcursor() as cur:
        cur.execute("""SELECT * FROM objects""" + tableName + """ WHERE check_word=%s AND id_object_types=%s LIMIT 1;""", (checkword_hash, objectType))
        objdb = cur.fetchall()
        
        if len(objdb) == 0:
            print "getObjectByCheckwordOnly not found"
            return None
              
        print "getObjectByCheckwordOnly:", str(objdb)        
        return objdb


def updateObjectOoid(tableName, ooid_hash, oid, checkword_hash, objectType):
    with getcursor() as cur:
        cur.execute("""UPDATE objects""" + tableName + """ t 
                       SET original_object_id = %s                       
                       WHERE id = %s AND check_word = %s AND id_object_types = %s; """, 
                       (ooid_hash, oid, checkword_hash, objectType))
        
        print "updateObjectOoid"

def updateObjectWithKeyData(tableName, ooid_hash, key, init_chain, cw_hash, object_type):
    with getcursor() as cur:
        cur.execute("""UPDATE objects""" + tableName + """ t
                       SET key = %s, init_chain = %s, check_word = %s
                       WHERE original_object_id = %s AND id_object_types = %s; """,
                       (key, init_chain, cw_hash, ooid_hash, object_type))

        print "updateObjectWithKeyData"

def updateObjectOoidForHotmailAttachment(tableName, ooid):
    with getcursor() as cur:
        cur.execute("""UPDATE objects""" + tableName + """ t
                       SET original_object_id = '-1'
                       WHERE original_object_id = %s AND id_object_types = 5; """,
                       (ooid,))

        print "updateObjectOoidForHotmailAttachment"


def deleteObjectByOoid(tableName, ooid_hash, objectType):
    with getcursor() as cur:
        cur.execute("""DELETE FROM objects""" + tableName + """ WHERE id_object_types=%s AND original_object_id=%s;""", (objectType, ooid_hash))
        
        print "deleteObjectByOoid"


def insertObject(tableName, ooid_hash, key, initChain, checkword_hash, isOwner, objectType):
    with getcursor() as cur:
        # tu se if ki checka type
        cur.execute("""INSERT INTO objects""" + tableName + """ (original_object_id, key, init_chain, check_word, is_owner, id_object_types) VALUES (%s, %s, %s, %s, %s, %s);""", (ooid_hash, key, initChain, checkword_hash, isOwner, objectType))
        cur.execute("""INSERT INTO objects""" + tableName + """ (original_object_id, key, init_chain, check_word, is_owner, id_object_types) VALUES (%s, %s, %s, %s, %s, %s);""", ("-1", key, initChain, checkword_hash, isOwner, objectType))
                    
        print "insertObject"


def insertAttDraft(att_id_hash, draft_id_hash, cw):
    with getcursor() as cur:
        cur.execute(""" INSERT INTO att_draft (attachment_id, draft_id, cw) VALUES (%s, %s, %s); """, (att_id_hash, draft_id_hash, cw))
        
        print "insertAttDraft"


def updateAttDraft(new_draft_id_hash, old_draft_id_hash):
    with getcursor() as cur:
        cur.execute(""" UPDATE att_draft SET draft_id = %s WHERE draft_id = %s; """, (new_draft_id_hash, old_draft_id_hash))

        print "updateAttDraft"


def updateAttDraftWithDraftId(att_id_hash, draft_id_hash):
    with getcursor() as cur:
        cur.execute(""" UPDATE att_draft SET draft_id = %s WHERE attachment_id = %s AND draft_id IS NULL; """, (draft_id_hash, att_id_hash))
        
        print "updateAttDraftWithDraftId att_id_hash=", att_id_hash, " draft_id_hash=", draft_id_hash


def deleteAttDraft(att_id_hash):
    with getcursor() as cur:
        cur.execute(""" DELETE FROM att_draft WHERE attachment_id = %s; """, (att_id_hash,))
        
        print "deleteAttDraft"


def existsAttDraft(att_id_hash):
    with getcursor() as cur:
        cur.execute(""" SELECT id FROM att_draft WHERE attachment_id = %s; """, (att_id_hash,))
        objdb = cur.fetchall()
        
        if len(objdb) == 0:
            return False
        else:
            return True


def getAttDraftByDraftId(draft_id_hash):
    with getcursor() as cur:
        cur.execute(""" SELECT attachment_id, cw FROM att_draft WHERE draft_id = %s; """, (draft_id_hash,))
        objdb = cur.fetchall()
        
        if len(objdb) == 0:
            print "getAttDraftByDraftId: None"
            return None
        
        print "getAttDraftByDraftId:", str(objdb)

        return objdb


def get_attdraft_by_attachment_id(att_id):
    with getcursor() as cur:
        cur.execute(""" SELECT attachment_id, cw FROM att_draft WHERE attachment_id = %s; """, (att_id,))
        objdb = cur.fetchall()

        if len(objdb) == 0:
            return None

        print "get_attdraft_by_attachment_id", str(objdb)
        return objdb

        
def getUserByService(serviceName):
    """ Returns None if not found or error"""
    
    user = None
        
    with getcursor() as cur:
        # change it because of hashing
        cur.execute("""SELECT id, username, password, u1, u2, u3, u4, u5, salt FROM users 
                       WHERE u1=%s OR u2=%s OR u3=%s OR u4=%s OR u5=%s;""", (serviceName, serviceName, serviceName, serviceName, serviceName))
        user_rs = cur.fetchall()                      

        if len(user_rs) == 0:
            print "User error (getUserFromDb): no user with service ", serviceName
            return None
        
        elif len(user_rs) > 1:
            print "User error (getUserFromDb): more then one user with service ", serviceName
            return None
                
        user = User()
        user.id = user_rs[0][0]
        user.username = user_rs[0][1]
        user.u1 = user_rs[0][3]
        user.u2 = user_rs[0][4]
        user.u3 = user_rs[0][5]
        user.u4 = user_rs[0][6]
        user.u5 = user_rs[0][7]
        user.salt = user_rs[0][8]
      
    return user


def determineObjectType(serviceName):
    if "@gmail.com" in serviceName:
        return 1
    elif "@hotmail.com" in serviceName:        
        return 2
    elif "@outlook.com" in serviceName:
        return 2
    elif "@windowslive.com" in serviceName:
        return 2
    
    return None


def determineObjectTypeForAttachments(object_type):
    if object_type == 1:    #GMAIL
        return 4            #GMAIL ATT
    elif object_type == 2:  #HOTMAIL
        return 5            #HOTMAIL ATT
    
    return None


"""
us = User()
us = getUser("goran", "a")
obj1 = getKeys(us, [{'ooid': 'a39fdb8cde2b9c93984ef30e8956825529c7880ba33ac628027c58e61ab3a6c81beec0bf081b051659652929511898a292ff09d5ee16b69329eab55f80bb3b18'}], 0)
#getKeys(us.id, [{'ooid': '16rWiO0_Ei6US-HVK63dnHpz4-2kVoeFZldEmzZaG6lM'}], 0) 

print obj1"""