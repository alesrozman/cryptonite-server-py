import random
import base64
import hashlib

from DBConnectionPool import getcursor

import sys

class User:
    def __init__(self):
        pass
    
    def getUserFromDb(self, usrnm, pswd):
        with getcursor() as cur:
            # change it because of hashing
            cur.execute("""SELECT * FROM users WHERE username=%s;""", (usrnm,))
            user = cur.fetchall()                       
    
            if len(user) == 0:
                print("User error (getUserFromDb): no user with username = ", usrnm, " and password = ", pswd)
                return None
            
            elif len(user) > 1:
                print("User error (getUserFromDb): more then one user with username = ", usrnm, " and password = ", pswd)
                return None
            
            #if user[0][2] != hashlib.sha512(base64.b64decode(user[0][8], '+-', True) + bytes(pswd, 'utf-8')).hexdigest():
            try:
                if user[0][2] != hashlib.sha512(base64.b64decode(user[0][8], '+-', True) + pswd).hexdigest():
                    return None
            except:
                print(sys.exc_info()[0])
        
            self.id = user[0][0]
            self.username = user[0][1]
            self.u1 = user[0][3]
            self.u2 = user[0][4]
            self.u3 = user[0][5]
            self.u4 = user[0][6]
            self.u5 = user[0][7]
            #self.googleUsername = user[0][3]
            #self.microsoftUsername = user[0][4]
            #self.facebookUsername = user[0][5]
            #self.yahooUsername = user[0][6]
            self.salt = user[0][8]
            
            return self
    
    def newUser(self, usrname, pswd, googleUsrname, microsoftUsrname, facebookUsrname, yahooUsrname):
        with getcursor() as cur:
            # later check for each username so you can return the problematic username
            cur.execute("""SELECT * FROM users WHERE username=%s OR u1=%s OR u2=%s OR u3=%s OR u4=%s;""", (usrname, googleUsrname, microsoftUsrname, facebookUsrname, yahooUsrname))
            users = cur.fetchall()
            
            if len(users) != 0:
                print('User error (newUser): one of the usernames already registered')
                return False
            
            slt = self.generateSalt()
            cur.execute("""INSERT INTO users (username, password, u1, u2, u3, u4, salt) VALUES (%s, %s, %s, %s, %s, %s, %s);""", (usrname, hashlib.sha512(base64.b64decode(slt, '+-') + bytes(pswd)).hexdigest(), googleUsrname, microsoftUsrname, facebookUsrname, yahooUsrname, slt,))
            
            cur.execute("""SELECT id FROM users WHERE username=%s;""", (usrname,))
            user = cur.fetchall()
            
            if len(user) == 0:
                print('User error (newUser): insert failed or something went wrong in insert')
                raise Exception()
            elif len(user) > 1:
                print('User error (newUser): inserted more then one user')
                raise Exception()
            
            print(user[0][0])
            
#             cur.execute("""CREATE TABLE public.objects""" + hashlib.sha512(base64.b64decode(slt, '+-') + bytes(str(user[0][0]))).hexdigest() + """ (id bigserial NOT NULL,
#                                                                                                                                                                     original_object_id varchar(256) NOT NULL,
#                                                                                                                                                                     key varchar(44) NOT NULL,
#                                                                                                                                                                     init_chain varchar(44) DEFAULT NULL,
#                                                                                                                                                                     check_word varchar(256) DEFAULT NULL,
#                                                                                                                                                                     is_owner boolean NOT NULL,
#                                                                                                                                                                     id_object_types int8 NOT NULL,
#                                                                                                                                                                     CONSTRAINT "%s" PRIMARY KEY (id,id_object_types));""", ('users' + str(user[0][0]) + '_id',))
            
#             cur.execute("""ALTER TABLE public.objects""" + hashlib.sha512(base64.b64decode(slt, '+-') + bytes(str(user[0][0]))).hexdigest() + """ ADD CONSTRAINT object_types_fk FOREIGN KEY (id_object_types)
#                             REFERENCES public.object_types (id) MATCH FULL
#                             ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;""")
            cur.execute(""" CREATE TABLE public.objects""" + str(user[0][0]) + """ 
                            (
                                id bigserial NOT NULL,
                                original_object_id varchar(256) NOT NULL,
                                key varchar(44) NOT NULL,
                                init_chain varchar(44) DEFAULT NULL,
                                check_word varchar(256) DEFAULT NULL,
                                is_owner boolean NOT NULL,
                                id_object_types int8 NOT NULL,
                                CONSTRAINT "%s" PRIMARY KEY (id,id_object_types)
                            ); """, ('users' + str(user[0][0]) + '_id',))
            
            cur.execute(""" ALTER TABLE public.objects""" + str(user[0][0]) + """ 
                            ADD CONSTRAINT object_types_fk FOREIGN KEY (id_object_types)
                                REFERENCES public.object_types (id) MATCH FULL
                                ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;""")
        
        return True
    
    
    # generate 64 long salt and put it in base 64 encoding
    def generateSalt(self):
        salt = []
        rand = random.SystemRandom()
        for i in range(64):
            salt.append(rand.randrange(0, 255, 1))
        
        return str(base64.b64encode(bytearray(salt), b'+-'))
    
    def __repr__(self):
        return 'id: ' + str(self.id) + '    username: ' + self.username + '   u1:  ' + str(self.u1) + '    u2: ' + str(self.u2) + '    u3: ' + str(self.u3) + '    u4: ' + str(self.u4) + '    u5: ' + str(self.u5) + '    salt: ' + self.salt


    def createObjectsTable(self, id):
        with getcursor() as cur:
            cur.execute(""" CREATE TABLE public.objects""" + str(id) + """ 
                            (
                                id bigserial NOT NULL,
                                original_object_id varchar(256) NOT NULL,
                                key varchar(44) NOT NULL,
                                init_chain varchar(44) DEFAULT NULL,
                                check_word varchar(256) DEFAULT NULL,
                                is_owner boolean NOT NULL,
                                id_object_types int8 NOT NULL,
                                CONSTRAINT %s PRIMARY KEY (id,id_object_types)
                            ); """, ('user' + str(id) + '_id',))
            
            cur.execute(""" ALTER TABLE public.objects""" + str(id) + """ 
                            ADD CONSTRAINT object_types_fk FOREIGN KEY (id_object_types)
                                REFERENCES public.object_types (id) MATCH FULL
                                ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;""")

#us = User()

#print(us.newUser('teoson', 'keksikeksi', 'teo.ivancic@gmail.com', None, None, None))
#us = us.getUserFromDb('teoson', 'keksikeksi')
#print(us)
#us = User()
#us.newUser("primoze", "a", "codeisgod@gmail.com", None, None, None)
