__author__ = 'bob'

import unittest
import json

import memcache

from model import User
from model import Object
from model import Session
from CryptoniteHandler import CryptoniteHandler


mc = memcache.Client(['127.0.0.1:11211'], debug=0)


class UserTestCase(unittest.TestCase):

    def test_user_creation_basic(self):
        user = User(1, "username", "pass", "salt", "gmail", "hotmail", None, None, None)
        self.assertEqual(1, user.id)
        self.assertEqual("username", user.username)
        self.assertEqual("pass", user.password)
        self.assertEqual("salt", user.salt)
        self.assertEqual("gmail", user.u1)
        self.assertEqual("hotmail", user.u2)
        self.assertEqual(None, user.u3)
        self.assertEqual(None, user.u4)
        self.assertEqual(None, user.u5)

    def test_user_creation_from_login(self):
        user = User.from_login("ales", "b")
        self.assertEqual(28, user.id)
        self.assertEqual("ales", user.username)
        self.assertEqual("ca127ddb40ac601e6b9a34861daa1ec4998ad017f040c29b31ce9a0d5daa087d9a001511420330ead9051674f788a433b906e0af5651347fd4fc3f33bd556074", user.password)
        self.assertEqual("S2qW7U3eXiKN55QMayTaCSyrWFrj+6rfbrZV9e8cIzW9HQO85gjBcexL4Ge8jjJZWQZYyOhXWU6dGdKXXQHjYw==", user.salt)
        self.assertEqual("ales.rozman.si@gmail.com", user.u1)
        self.assertEqual("rozman.ales@outlook.com", user.u2)
        self.assertEqual(None, user.u3)
        self.assertEqual(None, user.u4)
        self.assertEqual(None, user.u5)

    def test_user_creation_from_service(self):
        user = User.from_service("rozman.ales@outlook.com")
        self.assertEqual(28, user.id)
        self.assertEqual("ales", user.username)
        self.assertEqual(None, user.password)
        self.assertEqual("S2qW7U3eXiKN55QMayTaCSyrWFrj+6rfbrZV9e8cIzW9HQO85gjBcexL4Ge8jjJZWQZYyOhXWU6dGdKXXQHjYw==", user.salt)
        self.assertEqual("ales.rozman.si@gmail.com", user.u1)
        self.assertEqual("rozman.ales@outlook.com", user.u2)
        self.assertEqual(None, user.u3)
        self.assertEqual(None, user.u4)
        self.assertEqual(None, user.u5)


class ObjectTestCase(unittest.TestCase):

    def test_object_creation(self):
        obj = Object("ooid", "key", "init chain", "checkword", True, 1)
        self.assertEqual("ooid", obj.original_object_id)
        self.assertEqual("key", obj.key)
        self.assertEqual("init chain", obj.init_chain)
        self.assertEqual("checkword", obj.checkword)
        self.assertEqual(True, obj.is_owner)
        self.assertEqual(1, obj.object_type)


class SessionTestCase(unittest.TestCase):

    USERNAME = "ales"
    PASSWORD = "b"

    def test_session_creation(self):
        user = User.from_login(self.USERNAME, self.PASSWORD)
        s = Session(user)
        self.assertIsNotNone(s)

    def test_session_memcached(self):
        user = User.from_login(self.USERNAME, self.PASSWORD)
        s = Session(user)
        result = s.cache_session()
        self.assertTrue(result)
        s2 = Session.from_session_key(s.session_key)
        self.assertIsNotNone(s2)
        self.assertEqual(s.session_key, s2.session_key)


class HandlerTestCase(unittest.TestCase):

    LOGIN_JSON = json.dumps({"rty": "li", "lid": {"un": "ales", "p": "b", "psk": None}})

    def test_login(self):
        handler = CryptoniteHandler()
        result = json.loads(handler.handle_request(None, str(self.LOGIN_JSON)))
        self.assertIsNotNone(result["sek"])
        self.assertIsNotNone(result["ud"])
        self.assertEqual(result["ud"]["u1"], "ales.rozman.si@gmail.com")
        self.assertEqual(result["ud"]["u2"], "rozman.ales@outlook.com")
        self.assertIsNone(result["ud"]["u3"])
        self.assertIsNone(result["ud"]["u4"])
        self.assertIsNone(result["ud"]["u5"])

        # Check for session key in cache
        sek = str(result["sek"])
        s = Session.from_session_key(sek)
        self.assertIsNotNone(s)
        self.assertEqual(sek, s.session_key)

    def test_getkeys(self):
        # login
        handler = CryptoniteHandler()
        result = json.loads(handler.handle_request(None, str(self.LOGIN_JSON)))
        sek = str(result["sek"])

        # create getKeys request
        req = json.dumps(
            {"rty": "gk",
             "sek": sek,
             "ot": 1,
             "objs": [{"ooid": "14d0ab261dc0fe15",
                       "cw": "FF_no2224vlTCA"},
                      {"ooid": "14d0635474caa044",
                       "cw": "befNcZs55WZYouFE6Zz3Zq50"},
                      {"ooid": "14d01318190db39c",
                       "cw": "vUILpJByChN0SzVlAjXYvAMK3w"},
                      {"ooid": "14d0118f99a92023",
                       "cw": "IgbQUE6Mvr1wDnTFouo"},
                      {"ooid": "14d0118233a23764",
                       "cw": "IgbQUE6Mvr1wDnTFouo"}]
             })

        # handle getkeys request
        handler = CryptoniteHandler()
        result = json.loads(handler.handle_request(None, str(req)))
        self.assertIsNotNone(result)


if __name__ == '__main__':
    unittest.main()
